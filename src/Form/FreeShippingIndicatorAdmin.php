<?php

namespace Drupal\commerce_free_shipping_indicator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides free shipping indicator admin form.
 */
class FreeShippingIndicatorAdmin extends ConfigFormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CheckoutFlowBase object.
   *
   * @param \Drupal\Core\Entity\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_free_shipping_indicator_admin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_free_shipping_indicator.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_free_shipping_indicator.config');
    $promotion_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion');
    // $promotion_storage = $this->entityTypeManager->getStorage('commerce_promotion');
    $promotion = $config->get('promotion_reference') ? $promotion_storage->load($config->get('promotion_reference')) : '';

    $form['custom_amount'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use a custom free shipping amount'),
      '#default_value' => $config->get('custom_amount'),
    ];

    $form['promotion_reference'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'commerce_promotion',
      '#title' => $this->t('Promotion reference'),
      '#description' => $this->t('Use amount from an existing promotion. Make sure it uses the order price total condition'),
      '#size' => 30,
      '#maxlength' => 60,
      '#default_value' => $promotion,
      '#states' => [
        'visible' => [
          ':input[name="custom_amount"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $form['free_shipping_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Free shipping amount'),
      '#description' => $this->t('The amount needed to be eligible for free shipping'),
      '#default_value' => $config->get('free_shipping_amount'),
      '#states' => [
        'visible' => [
          ':input[name="custom_amount"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['ineligible_for_free_shipping'] = [
      '#type' => 'text_format',
      '#format' => $config->get('ineligible_for_free_shipping')['format'],
      '#title' => $this->t('Message when order is not eligible for free shipping'),
      '#description' => $this->t('A message that will be shown when the order is not eligible for free shipping. <br />
        Use the token @free-shipping-amount to include the amount left for the user to be eligible for free shipping. For example:<br />
        <b>Yor are @free-shipping-amount Away from Free Shipping.</b>'),
      '#default_value' => $config->get('ineligible_for_free_shipping')['value'],
      '#required' => TRUE,
    ];

    $form['eligible_for_free_shipping'] = [
      '#type' => 'text_format',
      '#format' => $config->get('eligible_for_free_shipping')['format'],
      '#title' => $this->t('Message when order is eligible for free shipping'),
      '#description' => $this->t('A message that will be shown when the order is eligible for free shipping.'),
      '#default_value' => $config->get('eligible_for_free_shipping')['value'],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if (!$form_state->getValue('custom_amount')) {
      $this->config('commerce_free_shipping_indicator.config')
        ->set('promotion_reference', $form_state->getValue('promotion_reference'))
        ->set('free_shipping_amount', '');
    }
    else {
      $this->config('commerce_free_shipping_indicator.config')
        ->set('promotion_reference', '')
        ->set('free_shipping_amount', $form_state->getValue('free_shipping_amount'));
    }

    $this->config('commerce_free_shipping_indicator.config')
      ->set('custom_amount', $form_state->getValue('custom_amount'))
      ->set('ineligible_for_free_shipping.value', $form_state->getValue('ineligible_for_free_shipping')['value'])
      ->set('ineligible_for_free_shipping.format', $form_state->getValue('ineligible_for_free_shipping')['format'])
      ->set('eligible_for_free_shipping.value', $form_state->getValue('eligible_for_free_shipping')['value'])
      ->set('eligible_for_free_shipping.format', $form_state->getValue('eligible_for_free_shipping')['format'])
      ->save();
  }

}
