<?php

namespace Drupal\commerce_free_shipping_indicator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_store\CurrentStore;
use Drupal\commerce_cart\CartProvider;

/**
 * Provides a Commerce free shipping block.
 *
 * @Block(
 *   id = "commerce_free_shipping_indicator_block",
 *   admin_label = @Translation("Free shipping indicator"),
 *   category = @Translation("Commerce")
 * )
 */
class FreeShippingIndicatorBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CurrentStore $current_store, CartProvider $cart_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentStore = $current_store;
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('commerce_store.current_store'),
      $container->get('commerce_cart.cart_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('commerce_free_shipping_indicator.config');
    $store = $this->currentStore->getStore();
    $cart = $this->cartProvider->getCart("default", $store);
    if ($cart && $cart->hasItems()) {
      $subtotal = $cart->getSubtotalPrice()->getNumber();

      if (!$config->get('custom_amount') && $config->get('promotion_reference')) {
        $promotionStorage = $this->entityTypeManager->getStorage('commerce_promotion');
        $promotion = $config->get('promotion_reference') ? $promotionStorage->load($config->get('promotion_reference')) : '';
        foreach ($promotion->conditions->getValue() as $condition) {
          if ($condition['target_plugin_id'] == 'order_total_price') {
            $operator = $condition['target_plugin_configuration']['operator'];
            $freeShippingNumber = $condition['target_plugin_configuration']['amount']['number'];
            if ($operator == '>=') {
              if ($subtotal < $freeShippingNumber) {
                $percentage = round($subtotal / $freeShippingNumber * 100);
                $amount_left = $freeShippingNumber - floor($subtotal);
              }
            }
            if ($operator == '>') {
              if ($subtotal <= $freeShippingNumber) {
                $percentage = round($subtotal / $freeShippingNumber * 100);
                $amount_left = $freeShippingNumber - floor($subtotal);
              }
            }

            return [
              '#theme' => 'commerce_free_shipping_indicator_block',
              '#free_shipping_amount' => isset($freeShippingNumber) ? $freeShippingNumber : 0,
              '#percentage' => isset($percentage) ? $percentage : 100,
              '#amount_left' => isset($amount_left) ? $amount_left : 0,
              '#currency' => $cart->getSubtotalPrice()->getCurrencyCode(),
              '#eligible_for_free_shipping' => $config->get('eligible_for_free_shipping.value'),
              '#ineligible_for_free_shipping' => $config->get('ineligible_for_free_shipping.value'),
            ];
          }
        }
      }

      if ($config->get('custom_amount') && $freeShippingNumber = $config->get('free_shipping_amount')) {
        if ($subtotal < $freeShippingNumber) {
          $percentage = round($subtotal / $freeShippingNumber * 100);
          $amount_left = $freeShippingNumber - floor($subtotal);
        }

        return [
          '#theme' => 'commerce_free_shipping_indicator_block',
          '#free_shipping_amount' => $freeShippingNumber,
          '#percentage' => isset($percentage) ? $percentage : 100,
          '#amount_left' => isset($amount_left) ? $amount_left : 0,
          '#currency' => $cart->getSubtotalPrice()->getCurrencyCode(),
          '#eligible_for_free_shipping' => $config->get('eligible_for_free_shipping.value'),
          '#ineligible_for_free_shipping' => $config->get('ineligible_for_free_shipping.value'),
        ];
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
